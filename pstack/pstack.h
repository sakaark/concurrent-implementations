#ifndef PSTACK_H
#define PSTACK_H

#include <atomic>

typedef struct Node {
  int data;
  std::atomic<bool> flag;
  int changed;
  struct Node *next;
} node;

typedef struct Stack {
  node *top;
  std::atomic<bool> flag;
  int changed;
} stack;

void createStack (stack *s);
int push (stack *s, int x);
int pop (stack *s, int (*f)(int));
void init_all();

#endif
