#include <atomic>
#include <cstdlib>
#include <cassert>
#include <pthread.h>
#include <cstdio>
#include "pstack.h"

int thread_0_res=0, thread_1_res=0;

extern stack *s;

int even_parity(int d) {
  return (d%2+1)%2;
}

int odd_parity(int d) {
  return d%2;
}

int trivial(int d) {
  return 1;
}

void *thread( void *e ) {
  thread_1_res = push(s, 8);
  return 0;
}

int main () {
  init_all();

  createStack(s);

  push(s, 3);
  push(s, 4);
  push(s, 5);

  pthread_t tid;
  pthread_create( &tid, 0, thread, 0 );
  thread_0_res = pop(s, even_parity);
  pthread_join( tid, 0 );

  int res1 = pop(s, trivial);
  
  //printf("Thread 0 result = %d\nThread 1 result = %d\n", thread_0_res, thread_1_res);
  assert ((res1 == 8 && thread_0_res == 4) || (res1 == 5 && thread_0_res == 8));
}
