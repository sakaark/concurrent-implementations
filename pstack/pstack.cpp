#include <atomic>
#include <cstdlib>
#include <cassert>
#include <pthread.h>
#include <cstdio>
#include "pstack.h"

#define DIVINE

using namespace std;

/*
  This macro waits till it sets the flag passed as argument.
  Parameters:
  FLAG: atomic bool object to be set
 */
#define FLAG_GET(flag) \
while (atomic_compare_exchange_strong(flag, negp, true) == false) { \
  neg = false; \
  negp = &neg; \
}

/*
  This macro is used to unset the flag passed as parameter
  Parameters:
  UNFLAG: atomic bool object which is to be unset
 */
#define UNFLAG_TRY(unflag) \
if (atomic_compare_exchange_strong(unflag, posp, false) == false) { \
  assert(1 == 0); \
}

bool pos = true;
bool neg = false;
bool *negp;
bool *posp;

atomic<bool> list_flag;

typedef struct List {
  node *n;
  struct List *next;
  struct List *prev;
} list;

list *listAll;

stack *s=NULL;

node *get_node_mem () {
  node *n = NULL;
  while (n == NULL) {
    n = (node *)malloc(sizeof(node));
  }
  list *l = NULL;
  while (l == NULL) {
    l = (list *)malloc(sizeof(list));
  }
  n->flag = false;
  n->changed = 0;
  l->n = n;
  l->next = listAll;
  l->prev = NULL;
  if (atomic_compare_exchange_strong(&list_flag, negp, true) == false) {	
    neg = false;							
    negp = &neg;							
    while (atomic_compare_exchange_strong(&list_flag, negp, true) == false) {
      neg = false;							
      negp = &neg;							
    }
  }
  listAll->prev = l;
  listAll = l;
  if (atomic_compare_exchange_strong(&list_flag, posp, false) == false) {	
    assert(1 == 0);
  }
  assert(n != NULL);
  return n;
}

void createStack (stack *s) {
  s->top = NULL;
  s->flag = false;
  s->changed = 0;
}

int push (stack *s, int x) {
  int j=9;
  node *n = get_node_mem();
  n->data = x;
  atomic<bool> *flag = &s->flag;

#ifdef DIVINE
  j = 10;
  FLAG_GET(flag);
#endif

  n->next = s->top;
  s->top = n;

#ifdef DIVINE
  UNFLAG_TRY(flag);
#endif

  return 0;
}

int pop (stack *s, int (*f)(int)) {
  atomic<bool> *flag = &s->flag;
#ifdef DIVINE
  FLAG_GET(flag);
#endif
  node *n = s->top;
  node *p = n;
  while (n != NULL) {
    int d1 = p->data, d2 = n->data;
    if (f(n->data) == 0) {
      p = n;
      atomic<bool> *flag2 = &p->flag;
#ifdef DIVINE
      FLAG_GET(flag2);
#endif
      n = n->next;
#ifdef DIVINE
      UNFLAG_TRY(flag);
#endif
      flag = flag2;
      continue;
    }
    if (p == n) {
      s->top = n->next;
    }
    else {
      atomic<bool> *flag2 = &n->flag;
#ifdef DIVINE
      FLAG_GET(flag2);
#endif
      p->next = n->next;
#ifdef DIVINE
      UNFLAG_TRY(flag2);
#endif
    }
#ifdef DIVINE
    UNFLAG_TRY(flag);
#endif
    return n->data;
  }
  return -1;
}

void init_all() {
  list_flag = false;
  negp = &neg;
  posp = &pos;
  while (s == NULL)
    s = (stack *)malloc(sizeof(stack));
  listAll = NULL;
  while (listAll == NULL) {
    listAll = (list *)malloc(sizeof(list));
  }
  listAll->next = NULL;
  listAll->prev = NULL;
  listAll->n = NULL;
}
