#include <atomic>
#include <cstdlib>
#include <cassert>
#include <pthread.h>
#include <cstdio>
#include "pstack.h"

int thread_0_res=0, thread_1_res=0;

extern stack *s;

int func1(int d) {
  if (d == 3) {
    return 1;
  }
  return 0;
}

int func2(int d) {
  if (d == 4) {
    return 1;
  }
  return 0;
}

int trivial(int d) {
  return 1;
}

void *thread( void *e ) {
  thread_1_res = pop(s, func2);
  return 0;
}

int main () {
  init_all();

  createStack(s);

  push(s, 3);
  push(s, 4);
  push(s, 5);

  pthread_t tid;
  pthread_create( &tid, 0, thread, 0 );
  thread_0_res = pop(s, func1);
  pthread_join( tid, 0 );

  int res1 = pop(s, trivial);
  int res2 = pop(s, trivial);
  
  //printf("Thread 0 result = %d\nThread 1 result = %d\n", thread_0_res, thread_1_res);
  assert (res1 == 5 && res2 == -1 && thread_0_res == 3 && thread_1_res == 4);
}
