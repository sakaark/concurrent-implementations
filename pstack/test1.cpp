#include <atomic>
#include <cstdlib>
#include <cassert>
#include <pthread.h>
#include <cstdio>
#include "pstack.h"

int thread_0_res=0, thread_1_res=0;

extern stack *s;

int trivial(int d) {
  return 1;
}

void *thread( void *e ) {
  thread_1_res = push(s, 4);
  return 0;
}

int main () {
  init_all();

  createStack(s);

  pthread_t tid;
  pthread_create( &tid, 0, thread, 0 );
  thread_0_res = push(s, 3);
  pthread_join( tid, 0 );

  int res1 = pop(s, trivial);
  int res2 = pop(s, trivial);
  
  //printf("Thread 0 result = %d\nThread 1 result = %d\n", thread_0_res, thread_1_res);
  assert ((res1 == 3 && res2 == 4) || (res1 == 4 && res2 == 3));
}
